local PART={}
PART.ID = "dtcaprotors"
PART.Name = "dtcaprotors"
PART.Model = "models/coreylz/smith-capaldi/rotors.mdl"
PART.AutoSetup = true
PART.Collision = true

if CLIENT then
	function PART:Initialize()
		self.posepos=0
		self.speed=0.1
	end

	function PART:Think()
		local ext=self.exterior
		if ext:GetData("flight") or ext:GetData("teleport") or ext:GetData("vortex") or ext:GetData("Float") then
			local target=1
			self.posepos=math.Approach(self.posepos,target,FrameTime()*self.speed)
			if self.posepos==target then
				self.posepos=0
			end
			self:SetPoseParameter("switch",self.posepos)
			self:InvalidateBoneCache()
		end
	end
end

TARDIS:AddPart(PART,e)