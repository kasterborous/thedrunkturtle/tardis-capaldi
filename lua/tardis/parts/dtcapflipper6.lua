local PART={}
PART.ID = "dtcapflipper6"
PART.Name = "dtcapflipper6"
PART.Model = "models/coreylz/smith-capaldi/Flipper.mdl"
PART.AutoSetup = true

PART.Animate = true
if SERVER then
	function PART:Initialize()
		self:SetColor(Color(255,255,255,255))
	end

	function PART:Use()
		self:EmitSound( "tardis/capaldi/Handbrake.wav")
		local exterior=self.exterior
		exterior:ToggleLocked()	
	end
end

TARDIS:AddPart(PART,e)