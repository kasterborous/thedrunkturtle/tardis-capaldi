local PART={}
PART.ID = "dtcapflight"
PART.Name = "dtcapflight"
PART.Model = "models/coreylz/smith-capaldi/handbrake.mdl"
PART.AutoSetup = true

PART.Animate = true

if SERVER then
	function PART:Initialize()
	end
	
	function PART:Use(activator)
		self:EmitSound("tardis/capaldi/Handbrake.wav" )
		self.exterior:ToggleFlight() 
	end
	
end

TARDIS:AddPart(PART,e)