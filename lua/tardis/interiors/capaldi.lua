-- Default

local T={}
local un={
	Model= "models/coreylz/exterior/exterior_uncloaked.mdl",
		Portal={
		pos=Vector(24,0,46),

	width=50,
	height=90,
	Fallback=
	{Vector(0,0,0),
	ang=Angle(0,90,0)},
		},
	
	Parts={
		door={model="models/coreylz/exterior/exterior_Door_uncloaked.mdl"
	}

	}
}

local red={
	Model= "models/coreylz/exterior/exterior.mdl",
		Portal={
		pos=Vector(24,0,46),

	width=40,
	height=90,
	Fallback=
	{Vector(0,0,0),
	ang=Angle(0,90,0)},
		},
	
	Parts={
		door={model="models/coreylz/exterior/Door.mdl",posoffset=Vector(-4,15.4,-42),angoffset=Angle(0,-60,0)}

	}
}

local def={
	Parts={
		door=true
	}
}

T.Base="base"
T.Name ="Capaldi"
T.ID="Capaldi"
T.Interior={
Model="models/coreylz/Smith-Capaldi/interior.mdl",
	Portal={
	pos=Vector(-345,0,143),
	ang=Angle(0,0,0),
	width=50,
	height=92
	},

	Fallback={
		pos=Vector(-325,0,103),
		ang=Angle(0,0,0)
	},
	Distance=600,
	IdleSound={
		{
			path="drmatt/tardis/interior_idle_loop.wav",
			volume=1	
		},
},
	Light={
		color=Color(255,255,255),
		pos=Vector(0,0,190),
		brightness=3
	},	

	

	Screens={
		{
		pos=Vector(15,50.4,162),
		ang=Angle(180,0,-90),
			width=485,
			height=250
		},
		{
		pos=Vector(-15,-50.4,162),
		ang=Angle(0,0,90),
			width=485,
			height=250
		}
	},
		Seats={
		{
			pos=Vector(43,157,109),
			ang=Angle(2,167,0)
		},
		{
			pos=Vector(161,28,109),
			ang=Angle(3,99,0)
		},		
		{
			pos=Vector(-142,-82,109),
			ang=Angle(-1,-61,0)
		},		
		{
			pos=Vector(9,-163,109),
			ang=Angle(-1,-2,0)
		},
	},
		Sounds={
		Teleport={
			demat="tardis/capaldi/dematext.wav",
			mat="tardis/capaldi/matext.wav"
		},
		Lock="tardis/capaldi/lock.wav",
		Door={
			enabled=true,
			open="tardis/capaldi/door_open.wav",
			close="tardis/capaldi/door_close.wav"
		},
		FlightLoop="tardis/capaldi/flight_loop.wav"
	},

Parts={
	dtcapconsole=true,
	dtcaptrcr={ang=Angle(0,-270,0)},
	dtcaprails=true,
	dtcapperim=true,
	dtcaprotors=true,
	dtcaphex=true,
	dtcapportals=true,
	dtcapstruts1=true,
	dtcapbook1=true,
	dtcapbook2=true,
	dtcapbook3=true,
	dtcapbook4=true,
	dtcapbook5=true,
	dtcapvents1=true,
	dtcapvents2=true,
	dtcaprndls1=true,
	dtcaprndls2=true,
	dtcapctwlktrm=true,
	dtcappermcnt1=true,
	dtcappermcnt2=true,
	dtcaplghths=true,
	dtsycabinets={
		ang=Angle(0,20,0)
	},
	dtsycabinets3={
		ang=Angle(0,100,0)
	},
		dtsycabinets4={
		ang=Angle(0,140,0)
	},
		dtsycabinets5={
		ang=Angle(0,180,0)
	},
		dtsycabinets6={
		ang=Angle(0,220,0)
	},
		dtsycabinets7={
		ang=Angle(0,260,0)
	},
			dtsycabinets8={
		ang=Angle(0,300,0)
	},
			dtsycabinets9={
		ang=Angle(0,340,0)
	},
	dtsycabinets2={
		ang=Angle(0,60,0)
	},

	dtcapwires=true,

	dtcapdtls1=true,
	dtcapdtls2={pos=Vector(0,0,0.02)},
	dtcapdtls4=true,
	dtcapdtls5=true,
	dtcapdtls6=true,
	dtcapdtls7={
	ang=Angle(0,90,0)
	},
	dtcaplights={
		ang=Angle(0,90,0)
	},
	dtcaptele1={
	ang=Angle(0,-90,0)
	},
	dtcaptele2={
	ang=Angle(0,90,0)
	},
	dtcapcntr={
	ang=Angle(0,29,0)
	},
	dtcapshft={	pos=Vector(0,0,9)
	},
	dtcapgrills=true,
	dtcapdemat1={
	pos=Vector(45,14.457,135),
	ang=Angle(80,90,0)
	},
	dtcapfloat={
	pos=Vector(-45,-13.5,137),
	ang=Angle(0,-90,20)
	},
	dtcapflight={
	pos=Vector(47.5,-17,135),
	ang=Angle(23,0,0)
	},
	dtcapdrsw={
	pos=Vector(-45,13.5,137),
	ang=Angle(0,-90,20)
	},
	dtcapflipper1={pos=Vector(-35,-5.82,135.5),
	ang=Angle(0,-90,0)},
	dtcapflipper2={pos=Vector(-35,-3.32,135.5),
	ang=Angle(0,-90,0)},
	dtcapflipper3={pos=Vector(-35,-1.32,135.5),
	ang=Angle(0,-90,0)},
	dtcapflipper4={pos=Vector(-35,1.32,135.5),
	ang=Angle(0,-90,0)},
	dtcapflipper5={pos=Vector(-35,3.32,135.5),
	ang=Angle(0,-90,0)},
	dtcapflipper6={pos=Vector(-35,5.32,135.5),
	ang=Angle(0,-90,0)},
	dtcapswitch={pos=Vector(-37.5,-12.41,137.5),
	ang=Angle(0,90,-21)},
	dtcaprndl1={ang=Angle(0,10,0)},
	dtcaprndl2={ang=Angle(0,10,0)},
	dtcpcbdr={
	pos=Vector(-45.2,-388,63.7),
	ang=Angle(-9,80,-20)
	},	dtcapbnr={
	pos=Vector(-33.4,-396.99,72.34),
	ang=Angle(91,180,90)
	},	
	dtcapshftgls={
		pos=Vector(0,0,35)
	},	
		door={
			model="models/doctorwho1200/capaldi/door.mdl",posoffset=Vector(-1,0,-52),angoffset=Angle(0,180,0)}

	}
}

T.Exterior={
	Model="models/doctorwho1200/capaldi/exterior.mdl",
	Mass=2900,
	Portal={
		pos=Vector(27.7,0,52.14),
		ang=Angle(0,0,0),
		width=50,
		height=92
	},
	Fallback={
		pos=Vector(35,0,5),
		ang=Angle(0,0,0)
	},
	Light={
		enabled=true,
		pos=Vector(0,0,122),
		color=Color(255,240,160)
	},
	Parts={
		door={
			model="models/doctorwho1200/capaldi/doorext.mdl",posoffset=Vector(-1,0,-52.1),angoffset=Angle(0,0,0)
		},
		vortex={
			model="models/doctorwho1200/toyota/2014timevortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,0,0),
			scale=10
		}
	}
}

TARDIS:AddInterior(T)